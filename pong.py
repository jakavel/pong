import colorama
from colorama import Cursor
#used to move the cursor to the top left corner of the screen every time a new frame is printed
pos = lambda y, x: Cursor.POS(x, y)

import sys
from math import sqrt, ceil

AI_ON = True #TODO: add 2 player vs.
AI_SKILL = 0.4 #how often the AI paddle moves towards the ball
#1 = always
#0 = never

#set both of these to False for the vanilla pong experience

RANDOM_ENABLED = True #whether the ball should bounce slightly unpredictably to keep things interesting
INERTIA_BOUNCING = True
#if the ball is hit with a moving paddle it will bounce in the paddle's direction
#eg. if my paddle is moving down while I hit the ball, the ball will bounce down even
#if it was going up before it hit my paddle

WIDTH = 79 #width if the playing board
HEIGHT = 23 #height of the playing board
PADDLE_SIZE = 5 #how many characters tall the paddle is
#will always be rounded to the nearest odd number because of how the paddle is drawn
BALL_SIZE = 2.5 #the radius of the ball
BALL_SIZE = BALL_SIZE ** 2

#change these two together so the paddle feels responsive
#and the game runs smoothly
FRAME_TIME = 0.1 #approximately how long each frame lasts
CHECK_NUMBER = 16 #how many times per frame the keyboard is checked

BALL_CHAR = "&" #character used to draw the ball
PADDLE_1_CHAR = "#" #character used to draw the left paddle
PADDLE_2_CHAR = '"' #character used to draw the right paddle

BOTTOM_TEXT = ["w and s to move", "t to quit"] #the text that gets cycled through on the bottom of the screen
WIN_SCORE = 4 #how many poinst to win the game

from pynput import keyboard
import os
os.system("clear") #looks nicer if there's nothing else in the command line

#make an empty file called key_presses
#it's purpose is to store which keys are pressed down
with open("key_presses", "w") as kp:
    pass

stop_checking = False
def check_keyboard(kp_lock):
    """Loop for detecting key presses and storing them in key_presses"""
    press = keyboard.Controller() #used to press buttons on the keyboard
    global stop_checking

    #the following is an endless loop
    #it loops every time a new event happenes
    #event = a key is pressed / released
    with keyboard.Events() as events:
        for event in events:
            if stop_checking:
                break
    
            #release the backspace since this script presses it at the end
            press.release(keyboard.Key.backspace)
            
            #key_presses is deleted at the end of the main thread
            #keeps this thread from encountering an error once the main one finishes
            if not os.path.isfile("key_presses"):
                continue

            #lock the key_presses file to avoid conflict with the main thread
            kp_lock.acquire()
            with open("key_presses", "r") as kp:
                pressed = kp.read()
            
            this_key = str(event.key) #string of the pressed key
            
            #if this event is about pressing down a key
            if "Press" in str(event) and not this_key in pressed:
                #add it to key_presses
                pressed += this_key

            #if this event is about releasing a key
            elif "Release" in str(event) and this_key in pressed:
                #remove it from key_presses
                pressed = pressed.replace(this_key, "")

            with open("key_presses", "w") as kp:
                kp.write(pressed)
            kp_lock.release()

            #press backspace to delete the pressed key from he command line
            #the if keeps this program from entering an endless loop since pressing
            #backspace makes an event
            if event.key != keyboard.Key.backspace:
                press.press(keyboard.Key.backspace)

from threading import Thread, Lock
kp_lock = Lock() #lock for the file key_presses

#run check_keyboard in a seperate thread since it'll loop endlessly
key_checker = Thread(target=check_keyboard, args=(kp_lock,))
key_checker.setDaemon(True) #daemon threads won't stop the program from terminating
#(this thread is automatically killed once the main thread finishes)
#tihs is done as a precaution since the thread is killed manually
key_checker.start()

def make_screen(ball_x, ball_y, paddle1_y, paddle2_y, right_score, left_score, bottom_text_pos):
    """Print a screen with the given properties."""
    global WIDTH, HEIGHT, PADDLE_SIZE, BOTTOM_TEXT, PADDLE_1_CHAR, PADDLE_2_CHAR, BALL_SIZE
    screen = [] #the scren that was printed as a list of strings (horizontal lines)
    for y in range(HEIGHT):
        this_line = ""
        for x in range(WIDTH):
            char = " "
            #formula for drawing the ball
            if (x - ball_x)**2 + (y - ball_y)**2 < BALL_SIZE:
                char = BALL_CHAR
            #draw the first paddle
            if x == 1 and abs(y-paddle1_y) < PADDLE_SIZE/2:
                char = PADDLE_1_CHAR
            #draw the second paddle
            if x == WIDTH - 2 and abs(y-paddle2_y) < PADDLE_SIZE/2:
                char = PADDLE_2_CHAR
            this_line += char
        
        screen.append(this_line)
        #exclude bottom text from screen so that the ball still bounces correcrly event with bottom text overlayed over it

        if y == HEIGHT - 1:
            #drawing the bottom text

            #calculate the distance between the left edge and the start of the bottom text
            half_width = WIDTH // 2
            half_width -= len(BOTTOM_TEXT[bottom_text_pos]) // 2

            #check if bottom text fits
            checker = half_width * 2 + len(BOTTOM_TEXT[bottom_text_pos])
            checker -= WIDTH
            if checker % 2 == 0:
                #if the missing amout of space is even
                left = half_width + checker//2
                right = half_width + checker//2
            else:
                #if it's odd 1 is added to the right side
                left = half_width + checker//2
                right = half_width + checker//2 + 1
            #print the following
            #the score for left + 
            #the normal line from the end of the score to the start of bottom text
            #the bottom text
            #the normal line from the end of bottom text to the character before last
            #the score for right
            this_line = str(left_score) +  this_line[1:left] + BOTTOM_TEXT[bottom_text_pos] + this_line[-right:-1] + str(right_score)
        #print the line
        print(this_line, end="\n")
        #make a list called screen for further procesing
    sys.stdout.flush() #just in case
    return screen

import time
import sys

#location and direction of the ball
ball_x = 15
ball_x_dir = 1
prev_ball_x_dir = 0
prev_ball_x_dir += ball_x_dir #previous direction of the ball

ball_y = 15
ball_y_dir = -1

#location of the paddles
paddle1_y = 10
paddle2_y = 10

#scores of players
right_score = 0
left_score = 0

def key_loop(frame_time):
     """A loop that is called instead of sleeping every frame. Checks if certain keys have been pressed."""
     global CHECK_NUMBER
     delta_paddle1 = 0 #how to move the paddle
     over = False #whether the game should end (if t was pressed)
     keys = [] 
    
     #checks if a key has been pressed CHECK_NUMBER of times
     #by default the game runs @ 10 fps, which is too slow for button presses to feel responsive
     #so they are checked multiple times a frame
 
     for i in range(CHECK_NUMBER):
        keys = []
        if os.path.isfile("key_presses"):
            #read which keys are pressed
            kp_lock.acquire()
            with open("key_presses", "r") as kp:
                keys = kp.read()
            kp_lock.release()

        keys = keys.split("'")

        if "t" in keys:
            over = True
            break
        if "w" in keys:
            delta_paddle1 = -1
        if "s" in keys:
            delta_paddle1 = 1
        
        time.sleep(frame_time/CHECK_NUMBER)
            
     return delta_paddle1, over

import random
def get_ai_move(ball_x, ball_y, paddle2_y, ball_x_dir):
    """Gets the move that the AI paddle should make."""
    global AI_SKILL, WIDTH

    choice = random.random()

    #calculate how long the ball has untill it reaches the paddle
    if ball_x_dir == -1:
        distance = WIDTH + ball_x
    else:
        distance = WIDTH - ball_x 
    
    #if the ball is further away than 0.9 of the width of the screen ignore it
    #this is done so that the AI paddle only chases the ball if it's coming towards it
    #this method is a little primitive, might need changing
    if distance > WIDTH * 0.9:
        return 0
    
    #get whether the AI paddle needs to move up or down to catch the ball
    if ball_y > paddle2_y:
        out = 1
    elif ball_y < paddle2_y:
        out = -1
    else:
        out = 0
    
    #if the choice is good enough move the paddle, otherwise do nothing
    if choice < AI_SKILL:
        return out
    else:
        return 0

over_from_score = False  #if the loop should be ended because someone won the game
over_from_button = False #if the loop should be ended because t was pressed
winner = "neither" #who won
time_counter = 0 #couns the number of frames since game start

#how the paddles were moved last turn
delta_paddle1 = 0
delta_paddle2 = 0


while not over_from_button and not over_from_score:
    print(pos(0, 0), end="") #move the cursor to the top left of the screen
    
    time_counter += 1
    
    #get winner
    if left_score >= WIN_SCORE:
        over_from_score = True
        BOTTOM_TEXT = ["left won"]
        winner = "left"
    elif right_score >= WIN_SCORE:
        over_from_score = True
        BOTTOM_TEXT = ["right won"]
        winner = "right"
    

    #switch to the next index in bottom text to display
    bottom_text_pos = time_counter // round( 6 / FRAME_TIME ) #every 6 seconds
    bottom_text_pos %= len(BOTTOM_TEXT)    
    
    #print a frame
    screen = make_screen(ball_x, ball_y, paddle1_y, paddle2_y, right_score, left_score, bottom_text_pos)
    
    #check if the ball should bounce from somewhere
    hit_paddle = False
    for line in screen:
        if PADDLE_1_CHAR + BALL_CHAR in line and ball_x < WIDTH / 2:
            #the ball hit the left paddle
            ball_x_dir = 1
            hit_paddle = True
            break
        elif BALL_CHAR + PADDLE_2_CHAR in line and ball_x > WIDTH / 2:
            #the ball hit the right paddle
            ball_x_dir = -1
            hit_paddle = True
            break
        elif line[0] == BALL_CHAR:
            #the ball hit the left edge
            ball_x_dir = 1
            right_score += 1
            break
        elif line[-1] == BALL_CHAR:
            #the ball hit the right edge
            ball_x_dir = -1
            left_score += 1
            break
    
    #if the ball was hit with a moving paddle
    #the ball bounces in the direction of the paddle
    if INERTIA_BOUNCING:
        if hit_paddle and ball_x_dir == 1 and delta_paddle1 != 0:
            ball_y_dir = delta_paddle1
        elif hit_paddle and ball_x_dir == -1 and delta_paddle2 != 0:
            ball_y_dir = delta_paddle2
    
    #if the ball somehow lost it's y momentum and hits a paddle give it momentum       
    elif hit_paddle and ball_y_dir == 0:
        ball_y_dir = random.choice([1, -1])
    elif hit_paddle and RANDOM_ENABLED:
        y = ball_y_dir
        #add a bit of randomness to the ball's bounces to make it more interesting
        ball_y_dir = random.choice([y, y, y, y, y, y, 0, 1, -1])
    
    #check if the ball bounced from the top / bottom of the screen 
    hit_edge = False
    if BALL_CHAR in screen[0]:
        ball_y_dir = 1
        hit_edge = True
    elif BALL_CHAR in screen[-1]:
        ball_y_dir = -1
        hit_edge = True

    if hit_edge and ball_x_dir != 0 and RANDOM_ENABLED:
        #add a bit of randomness to the ball's bounces to make it more interesting
        x = 0
        x += ball_x_dir
        ball_x_dir = random.choice([x, x, x, x, 0])
        if ball_x_dir == 0:
            prev_ball_x_dir = x
        
    #if the ball lost it's x momentum because of the previous if
    #bouche it in the direction it was previously going in so that
    #the same player doesn't have to bounce the ball twice in a row    
    elif hit_edge and ball_x_dir == 0:
        ball_x_dir = prev_ball_x_dir

    #move the ball
    ball_x += ball_x_dir
    ball_y += ball_y_dir
    
    #AI moves its paddle
    delta_paddle2 = get_ai_move(ball_x, ball_y, paddle2_y, ball_x_dir)
    paddle2_y += delta_paddle2

    #move the paddle
    delta_paddle1, over_from_button = key_loop(FRAME_TIME) #key_loop() also takes the role of a time.sleep()
    #move the player's paddle
    paddle1_y += delta_paddle1
    paddle1_y = sorted([0, paddle1_y, HEIGHT])[1]
    #clamp the value of paddle1_y between 0 and HEIGHT
    #this is done so that the baddle can't be moved off screen

#clean up after yourself
os.system("clear")

stop_checking = True
key_checker.join()

if winner == "left":
    print("__               ____   ____   ___________ ")
    print("\ \             |  __| |  __| |____   ____|")
    print(" \ \            | |    | |         / /     ")
    print("  \ \           | |__  | |__      / /      ")
    print("   \ \          |  __| |  __|    / /       ")
    print("    \ \         | |    | |      / /        ")
    print("     \ \______  | |__  | |     / /         ")
    print("      \_______| |____| |_|    /_/          ")

if winner == "right":
    print("       _____   _   ______   _    _   ___________  ")
    print("      / __  / | | |  ____| | |  | | |____  _____| ")
    print("     / /_/ /  | | | |      | |  | |      \ \      ")
    print("    /  ___/   | | | | ___  | |__| |       \ \     ")
    print("   /  \       | | | ||_  | |  __  |        \ \    ")
    print("  / /\ \      | | | |  | | | |  | |         \ \   ")
    print(" / /  \ \     | | | |__| | | |  | |          \ \  ")
    print("/_/    \_\    |_| |______| |_|  |_|           \_\ ")

if winner == "left" or winner == "right":
    print("__                     __  ______   __     _     ")
    print("\ \                   / / |  __  | |  \   | |    ")
    print(" \ \       ___       / /  | |  | | |   \  | |    ")
    print("  \ \     / _ \     / /   | |  | | |    \ | |    ")
    print("   \ \   / / \ \   / /    | |  | | | |\  \| |    ")
    print("    \ \_/ /   \ \_/ /     | |  | | | | \    |    ")
    print("     \   /     \   /      | |__| | | |  \   |    ")
    print("      \_/       \_/       |______| |_|   \__|    ")

os.remove("key_presses") #remove the file used to store key presses
